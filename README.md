# Noctua - Lung nodule detection algorithm
Developed by: Aneesh Muppidi, Daniyaal Qazi, and Amol Kumar.

![Build Status] (https://img.shields.io/teamcity/codebetter/bt428.svg)
![License] (https://img.shields.io/github/license/mashape/apistatus.svg)

# Lung Cancer
Lung cancer is one of the most aggressive cancers and is projected by the American Cancer Society to result in mortality of over 70% with approximately 225,000 Americans newly diagnosed in 2016. It is responsible for roughly, 25% of all cancer deaths. The early identification of pulmonary nodules are an important task for the management of lung cancer; If detected early, malignant nodules can be treated with a much higher success rate. Only 16% of lung cancer cases are diagnosed early.
    However, reading of computed tomography (CT) scans by radiologists for detecting the presence of pulmonary nodules and their malignancy is a tedious and time-consuming task. Furthermore, the accuracy rate of radiologists and modern radiology methods are as it follows (Data is according to 2015 New York Screening):
    
 - 52% for nodules in size of ≤ 4mm. 
 - 65% for nodules in size of 4-6mm.
 - 76% for nodules in size of 8mm.


The detection of nodules also requires expensive software to aid in the detection of lung nodules. These expensive software programs are not very accessible in 3rd world countries. The unavailability of these software programs, further decreases the accuracy rate of lung nodule detection for radiolgosits.

Noctua was born to achieve a much higer accuracy in the detection of nodules and to reduce the false-positive rate. It was designed in such a way that anybody could use our algorithm. Noctua uses novel artifical neural networks and recent advances such as a "Genetic Support Vector Machine". 

Futhermore, Noctua was created for the [Ecybermission](http://www.ecybermission.com/) competition.

# Features

  - Can take in format(s)  [ DICOM ]   *more formats will come soon.
  - Full support for LIDC-IDRI
  - Output Segmented CT slices
  - Output detected nodules

# Requirements 

 Noctua uses MATLAB. 
 Tested on OSX however the MATLAB syntax is the same across all platforms, which allows other platforms that support MATLAB* to run Noctua.
 
 
 *Noctua has not been tested on GNU Octave, support for Octave is not guaranteed.

Requirements include: 

> Matlab Ra 2013 IDE or a more recent version. [Matlab](https://www.mathworks.com/products/matlab.html)
> 
> OSX, Windows, and Linux.
> 
> Matlab Image processing toolbox.
> 
> LIDC-IDRI dataset or similarly formatted scans. 


### LIDC-IDRI
The Lung Image Database Consortium image collection (LIDC-IDRI) consists of diagnostic and lung cancer screening thoracic computed tomography (CT) scans. The dataset is initiated by the National Cancer Institute (NCI), further advanced by the Foundation for the National Institutes of Health (FNIH), and accompanied by the Food and Drug Administration (FDA) through active participation. Seven academic centers and eight medical imaging companies collaborated to create this data set which contains 1018 cases. Containing nodules from 3mm to 8mm in size (Early stages of lung cancer) Each subject includes images from a clinical thoracic CT scan and an associated XML file that records the results of a two-phase image annotation process performed by four experienced thoracic radiologists.

The algorithm is tested on this dataset. Furthermore, the classifier was also trained on 70% of the dataset.

You can download the dataset by following the instructions on the [LIDC-IDRI Website](https://wiki.cancerimagingarchive.net/display/Public/LIDC-IDRI)
![Screen Shot 2017-02-20 at 3.25.53 PM.png](https://www.dropbox.com/s/zwp7n17deblh1e0/Screen%20Shot%202017-02-20%20at%203.25.53%20PM.png?dl=0&raw=1)


### How to run
1) Download the repository.

2) Open  ```main.m```

3) Change 
```
path_data = ['/Users/USER/Downloads/LIDC-IDRI/LIDC-IDRI']; 
```
to the path which either includes the LIDC-IDRI dataset or your DICOM files.

For example after we download the dataset,
It will look something like this (On OSX, but it is the same steps for Windows):

![Screen Shot 2017-02-20 at 3.52.06 PM.png](https://www.dropbox.com/s/x8bsq6bgroagvj0/Screen%20Shot%202017-02-20%20at%203.52.06%20PM.png?dl=0&raw=1)

However, you will not input this path as the ``` path_data ```.

Inside this folder is located another folder named ```LIDC-IDRI ```

![Screen Shot 2017-02-20 at 3.55.39 PM.png](https://www.dropbox.com/s/2u76tms9yacq2ph/Screen%20Shot%202017-02-20%20at%203.55.39%20PM.png?dl=0&raw=1)
This is the correct folder for the input. 

For example my input for  ```path_data```would be ``` path_data = ['/Users/Noctua/Downloads/LIDC-IDRI/LIDC-IDRI']; ```


4) After this edit to```main.m```, click the run button.
The results may take some time depending on the system you run Noctua on.

5) After the run is completed, type in the command line


```imshow3Dfull(nodule_img_3d); ```
  

   Which will output the detected nodules.
  To view each segmented lung region from it's respective CT slice type in the command line
  
```imshow3Dfull(lung_seg_img_3d); ```


  
Note - We are grateful for using the [imshow3Dfull](https://www.mathworks.com/matlabcentral/fileexchange/47463-imshow3dfull--3d-imshow-in-3-views-?requestedDomain=www.mathworks.com) library which was developed and licensed by Maysam Shahedi.


### Development

Want to contribute? Great!
We hoped by open-sourcing Noctua, other developers can make it even better! You can start by forking our repository and adding your improvements. We will recieve a notification then we will review your repository and merge it with our repository. 

### Coming soon

 - More supported formats
 - Possibly a GUI application
 - Automatic outputs 
 - Support for multiple languages

### License
----

Massachusetts Institute of Technology (MIT) License. 



